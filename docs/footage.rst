Footage
################################################
**Description**

To use a footage item from the Project panel as an object in an expression, use the global footage method, as in ``footage("file_name")``. You can also access a footage object using the source attribute on a layer whose source is a footage item.

----

Footage.width
*********************************************
**Description**

Returns the width of the footage item, in pixels.

**Type**

Number

----

Footage.height
*********************************************
**Description**

Returns the height of the footage item, in pixels.

**Type**

Number

----

Footage.duration
*********************************************
**Description**

Returns the duration of the footage item, in seconds.

**Type**

Number

----

Footage.frameDuration
*********************************************
**Description**

Returns the duration of a frame in the footage item, in seconds.

**Type**

Number

----

Footage.ntscDropFrame
*********************************************
**Description**

Returns true if the timecode is in drop-frame format. (After Effects CS5.5 and later.)

**Type**

Boolean

----

Footage.pixelAspect
*********************************************
**Description**

Returns the pixel aspect ratio of the footage item.

**Type**

Number

----

Footage.name
*********************************************
**Description**

Returns the name of the footage item as shown in the Project panel.

**Type**

String
